> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

LIS 4331

Quentin Coombs

LIS 4331 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio and create first app
    - Provide screenshots of installations
    - Create bitbucket repo
    - Complete Bitbucket Tutorials
    (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Tip Calculator App
    - Use spinner control to create drop-down menus
    - Create and utilize Launcher Icon
    - Provide Screenshot of Unpopulated interface
    - Provide Screenshot of Populated interface
    - Use java code If Else statement to make calculations
    - Add an image onto app 
    - Change background color from default 

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Converter App
    - Use radio button to create options
    - Create and utilize Launcher Icon
    - Provide Screenshot of Unpopulated interface
    - Provide screenshot of Toast notification
    - Provide Screenshot of Populated interface
    - Use java code If Else statement to make calculations
    - Add text color and other design elements
    - Change background color from default 

4. [P1 README.md](p1/README.md "My A3 README.md file")
    - Create music player app
    - Use images and buttons to pause and play music
    - Add files to res folder
    - Create and utilize launcher icon
    - Provide screenshots of playing interface
    - Provide screenshots of paused interface
    - Provide screenshots of splash screen

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create mortgage calculator app
    - Create splash screen
    - Create an app using multiple activities and learning how they work accordingly
    - Learned how to add pictures for each option a user selects
    - Create a launcher icon
    - Provide screenshots of running app, splash, error and working screen.
    - Answer questions from chapters 11 & 12

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create an app that displays RSS feed
    - Use multiple activities and methods to create a cohesive app
    - Answered questions from the textbook and Canvas
    - Provide screenshots of splash screen and running app

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create a Task List Application that utilizes a database
    - Create a app icon and splash screen that is used to initalize resources
    - Change the color background so that it is aesthetically pleasingv
    - Learned how to access a database within a mobile application that is used to add and delete rows and columns
    - Answered questions from PDF on cavas
    - Provide screenshots of splash screen and running app
    





