> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

LIS 4331

Quentin Coombs

### Assignment 2 Requirements:

*Sub-Heading:*

1. Create a tip calculator using the same skills learned from the conert ticket app from chapter 3
2. Use the spinner widget to create drop down menus, and create and utilize a launcher icon
3. Answered questions from chapters 3 & 4


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates new GIT repository
2. git status- displays the state of the working directory and the staging area 
3. git add- adds files to the staging area
4. git commit- saves changes to your local repository
5. git push- publishes new local cmmits on a remote server
6. git pull- fetches and downwloads content from a remote repository and updates the local repository to match that content
7. mkdir- make new directory

#### Assignment Screenshots:

*Screenshot of unpopulated user interface running*:

![Unpopulated user interface Screenshot](img/unpop.png)

*Screenshot of populated user interface running*:

![Populated user interface Screenshot](img/pop.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
