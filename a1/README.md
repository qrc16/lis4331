> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>


LIS 4331


Quentin Coombs 

### Assignment 1 Requirements:

*Four parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App 
* Screenshots of running Android Studio - Contacts App
* git commands with short descriptions 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- creates new GIT repository
2. git status- displays the state of the working directory and the staging area 
3. git add- adds files to the staging area
4. git commit- saves changes to your local repository
5. git push- publishes new local cmmits on a remote server
6. git pull- fetches and downwloads content from a remote repository and updates the local repository to match that content
7. mkdir- make new directory

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

*Screenshots of running Android Studio - Contacts App*:
![Android Studio Contact Apps Screenshot](img/contacts1.png) 
![Android Studio Contact Apps Screenshot](img/contacts2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/qrc16/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/qrc16/myteamquotes/ "My Team Quotes Tutorial")
