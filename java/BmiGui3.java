 // A GUI to compute a person's body mass index (BMI).
 // Final version with event handling.

 import java.awt.*;
 import java.awt.event.*;
 import javax.swing.*;

 public class BmiGui3 implements ActionListener {
 // BmiGui3 is its own runnable client program
 public static void main(String[] args) {
 BmiGui3 gui = new BmiGui3();
 }

 // onscreen components stored as fields
 private JFrame frame;
 private JTextField LegAField;
 private JTextField LegBField;
 private JLabel LegCField;
 private JButton computeButton;

 public BmiGui3() {
 // set up components
 LegAField = new JTextField(5);
 LegBField = new JTextField(5);
 LegCField = new JLabel("Type ");
 computeButton = new JButton("Compute distance for Leg C:");

 // attach GUI as event listener to Compute button
 computeButton.addActionListener(this);

 // layout
 JPanel north = new JPanel(new GridLayout(2, 2));
 north.add(new JLabel("Leg A: "));
 north.add(LegAField);
 north.add(new JLabel("Leg B: "));
 north.add(LegBField);

 // overall frame
 frame = new JFrame("Leg C: ");
 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 frame.setLayout(new BorderLayout());
 frame.add(north, BorderLayout.NORTH);
 frame.add(LegCField, BorderLayout.CENTER);
 frame.add(computeButton, BorderLayout.SOUTH);
 frame.pack();
 frame.setVisible(true);
 }

 // Handles clicks on Compute button by computing the BMI.
 public void actionPerformed(ActionEvent event) {
 // read height/weight info from text fields
 String LegAText = LegAField.getText();
 double LegA = Double.parseDouble(LegAText);
 String LegBText = LegBField.getText();
 double LegB = Double.parseDouble(LegBText);

 // compute BMI and display it onscreen
 double LegC = (LegA*LegA) + (LegB*LegB);
 double LegCFinal = Math.sqrt(LegC);
 LegCField.setText("Leg C: " + LegCFinal);
 }
 }