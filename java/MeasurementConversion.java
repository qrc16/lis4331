import java.util.Scanner;

public class MeasurementConversion 
{

    public static void main(String []args)
    {
        int inches;
        final double INCHES_TO_CENTIMETERS = 2.54;
        final double INCHES_TO_METERS = 39.37;
        final double INCHES_TO_FEET = 12;
        final double INCHES_TO_YARDS = 36;
        final double FEET_TO_MILES = 5280;
        double centimeters;
        double meters;
        double feet;
        double yards;
        double miles;

        Scanner read = new Scanner(System.in);

        System.out.printf("Please enter number of inches: ");

        while (!read.hasNextInt()){
            System.out.println("Not valid integer!");
            read.next();
            System.out.print("Please enter number of inches: ");
        }
        inches = read.nextInt();

        centimeters = inches * INCHES_TO_CENTIMETERS;

        meters = inches / INCHES_TO_METERS;

        feet = inches / INCHES_TO_FEET;

        yards = inches / INCHES_TO_YARDS;

        miles = (inches * .0833184) / FEET_TO_MILES;

        System.out.println(inches + " inch(es) equals: ");
        System.out.println(centimeters + " centimeter(s)");
        System.out.println(meters + " meter(s)");
        System.out.println(feet + " feet");
        System.out.println(yards + " yard(s)");
        System.out.println(miles + " mile(s)");


        
    }
}

       