import java.util.Scanner;

public class Percentage2 {
public static void main(String[] args) {

    Scanner input = new Scanner(System.in);
    double obtainedScore=0, totalScore=0;
    float Percentage;

    System.out.println("Enter Tip Percentage: ");
    obtainedScore = input.nextDouble();
    System.out.println("Enter Total Bill: ");
    totalScore = input.nextDouble();

    Percentage = (float) (totalScore * (obtainedScore/100));

    System.out.println("Each guest should tip: " + "$" +  Percentage);

    }
}