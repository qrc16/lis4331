class Book extends Product1{

    private String author;

    public Book(){
        super();
        System.out.println("\nInside book default constructor.\n");

        author = "John Doe";

    }

    public Book(String cd, String desc, double pr, String auth){

        super(cd, desc, pr);
        System.out.println("\nInside book constructor with parameters\n");
        
        author = auth;

    }

    public String getAuthor(){
        return author;
    }

    public void setAuthor(String auth){
        author = auth;
    }
    public void print(){
        super.print();
        System.out.println("\nAuthor: " + author);


    }
}