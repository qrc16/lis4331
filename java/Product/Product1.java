public class Product1{

    private String code;
    private String description;
    private double price;

    public Product1(){
        code = "abc123";
        description = "My Widget";
        price = 49.99;

        System.out.println("\nInside product default constructor.\n");

    }

    public Product1(String cd, String desc, double pr){
        
        System.out.println("\nInside product constructor with parameters\n");
        this.code = cd;
        this.description = desc;
        this.price = pr;

    }

    public void setCode(String cd){
        code = cd;
    
    }

    public String getCode(){
        return code;

    }

    public void setDescription(String desc){
        description = desc;
    
    }

    public String getDescription(){
        return description;
    
    }

    public void setPrice(double pr){
        price=pr;

    }

    public double getPrice(){
        return price;

    }

    public void print(){
        System.out.println("\nCode: " + code + ", Description: " + description + ", Price: " + price);


    }
}