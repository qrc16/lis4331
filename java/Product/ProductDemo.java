import java.util.Scanner;

class ProductDemo
{
    public static void main(String[]args)
    {
        String code = "";
        String description = "";
        double price = 89.99;
        Scanner sc = new Scanner(System.in);
    

        System.out.println("\n/////Below are default constructor values://///\n");
        Product1 p1 = new Product1();
        System.out.println("\nCode: " + p1.getCode() +
                            "\nDescription: " + p1.getDescription() +
                            "\nPrice: " + p1.getPrice() + "\n");

        System.out.println("\n/////Below are user entered values://///");

        //get user input
        System.out.print("\nCode: ");
        code = sc.nextLine();

        System.out.print("Description: ");
        description = sc.nextLine();

        System.out.print("Price: ");
        price = sc.nextDouble();

        Product1 p2 = new Product1(code, description, price);
        System.out.println("\nCode: " + p2.getCode() + "\nDescription: " + p2.getDescription() + "\nPrice: " + p2.getPrice() + "\n");

        System.out.println("\n////Below using setter methods to pass literal values, then print() method to display values://///");
        p2.setCode("xyz789");
        p2.setDescription("Test Widget");
        p2.setPrice(89.99);
        p2.print();



    }
}
