import java.util.Scanner;

public class SimpleInterestCalculator
{

    public static void main(String []args)
    {
        int years;
        double Principal;
        double interestRate;
    
        Scanner read = new Scanner(System.in);

        System.out.print("Current Principal: ");

        while (!read.hasNextInt()){
            System.out.println("Not valid number!");
            read.next();
            System.out.print("Please try again. Enter Principal: ");
        }
        Principal = read.nextDouble();

        System.out.print("Interest Rate (per year): ");

        while(!read.hasNextDouble()){
            System.out.println("Not valid number!");
            read.next();
            System.out.print("Please try again. Enter Interest Rate: ");
        }
        interestRate = read.nextDouble();

        System.out.print("Total time (in years): ");

        while(!read.hasNextInt()){
            System.out.println("Not valid integer!");
            read.next();
            System.out.print("Please try again. Enter years: ");
        }
        years = read.nextInt();
        amountSaved = Principal * (1 + (years*interestRate));
        System.out.print("You will have saved" + amountSaved + "in" + years + "years" + "at an interest rate of" + interestRate + "%");
        read.close();
    }

}



       