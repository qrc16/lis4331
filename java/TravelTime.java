import java.util.Scanner;
import java.lang.System;
import java.text.DecimalFormat;

public class TravelTime
{
    public static void main(String[] args)
    {
        //variables
        double miles = 0;
        double MPH = 0;
        boolean valid;
        boolean valid2;

        //scanner
        Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        //beginning code
        System.out.println("Program calculates approximate travel time.");
        System.out.print("\n");

        //do while loop x2
        do{
            System.out.print("\n");
            System.out.print("Please enter miles from location: ");

            if (input.hasNextDouble()) 
            {
                miles = input.nextDouble();
                if (miles <= 3000 && miles > 0){
                    
                    valid = true;

                }else{

                    System.out.println("Miles must be greater than 0, and no more than 3000.");
                    valid = false;

                }//end else

            }else{
            
                System.out.println("Invalid double--Miles must be a number. ");
                valid = false;
                input.next();

            }//end else

        }while (!(valid));

        do{
            System.out.print("\n");
            System.out.print("Please enter MPH: ");

            if (input2.hasNextDouble()) 
            {
                MPH = input2.nextDouble();
                if (MPH <= 100 && miles > 0){
                    
                    valid2 = true;

                }else{

                    System.out.println("MPH must be greater than 0, and no more than 100.");
                    valid2 = false;

                }//end else

            }else{
            
                System.out.println("Invalid double-- MPH must be a number. ");
                valid2 = false;
                input2.next();

            }//end else

        }while (!(valid2));

        //calculations

        double MPM = MPH / 60;
        double time = miles / MPM;

        int hours = (int) time / 60;
        int minutes = (int) time % 60;;

        DecimalFormat df = new DecimalFormat("00");

        //print to screen
        
        System.out.print("\n");
        System.out.println("Estimated travel time: " + df.format(hours) + " hr(s) " + df.format(minutes) + " minutes.");

    
    }//end main
}//end TravelTime