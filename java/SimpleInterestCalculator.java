import java.text.NumberFormat;
import java.util.Scanner;

public class SimpleInterestCalculator
{

    public static void main(String []args)
    {
        int years;
        double Principal;
        double interestRate;
        double amountSaved;
    
        Scanner read = new Scanner(System.in);

        System.out.print("Current Principal: ");

        while (!read.hasNextInt()){
            System.out.println("Not valid number!");
            read.next();
            System.out.print("Please try again. Enter Principal: $");
        }
        Principal = read.nextDouble();

        System.out.print("Interest Rate (per year): ");

        while(!read.hasNextDouble()){
            System.out.println("Not valid number!");
            read.next();
            System.out.print("Please try again. Enter Interest Rate: ");
        }
        interestRate = read.nextDouble();

        System.out.print("Total time (in years): ");

        while(!read.hasNextInt()){
            System.out.println("Not valid integer!");
            read.next();
            System.out.print("Please try again. Enter years: ");
        }
        years = read.nextInt();
        amountSaved = Principal * (1 + (years*(interestRate/100)));

    NumberFormat nf = NumberFormat.getCurrencyInstance();

        System.out.print("You will have saved " + nf.format(amountSaved) + " in " + years + "years " + " at an interest rate of " + interestRate + "%");
        read.close();
    }

}



       